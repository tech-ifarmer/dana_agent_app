package com.parallax.showofff.api

import android.util.Log
import com.google.gson.Gson
import com.misfit.dana.models.*
import com.parallax.showofff.utils.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object WebService {

    const val ON_FAILURE_MESSAGE = "Please Check your Internet Connection"

    private fun toRequestBody(value: String?): RequestBody {
        return if (value != null) {
            RequestBody.create(MediaType.parse("text/plain"), value)
        } else {
            RequestBody.create(MediaType.parse("text/plain"), "")
        }

    }

    fun userBypass(
        body: BodySendOtp,
        callBack: (ApiResponse<ResponseLogin>) -> Unit
    ) {

        apiServices.userBypass(body).enqueue(object : Callback<ResponseLogin> {
            override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                Log.d("userBypass", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLogin>,
                response: Response<ResponseLogin>
            ) {
                Log.d("userBypass", Gson().toJson(response.body()))
                Log.d("userBypass", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("userBypass", response.code().toString())

                }
            }

        })
    }

    fun sendOtp(
        body: BodySendOtp,
        callBack: (ApiResponse<ResponseSendOtp>) -> Unit
    ) {

        apiServices.sendOtp(body).enqueue(object : Callback<ResponseSendOtp> {
            override fun onFailure(call: Call<ResponseSendOtp>, t: Throwable) {
                Log.d("sendOtp", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseSendOtp>,
                response: Response<ResponseSendOtp>
            ) {
                Log.d("sendOtp", Gson().toJson(response.body()))
                Log.d("sendOtp", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("sendOtp", response.code().toString())

                }
            }

        })
    }

    fun login(
        body: BodyLogin,
        callBack: (ApiResponse<ResponseLogin>) -> Unit
    ) {

        apiServices.login(body).enqueue(object : Callback<ResponseLogin> {
            override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                Log.d("login", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLogin>,
                response: Response<ResponseLogin>
            ) {
                Log.d("login", Gson().toJson(response.body()))
                Log.d("login", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("login", response.code().toString())

                }
            }

        })
    }

    fun loanCharge(
        body: BodyLoanCharge,
        callBack: (ApiResponse<ResponseLoanCharge>) -> Unit
    ) {

        apiServices.loanCharge(body).enqueue(object : Callback<ResponseLoanCharge> {
            override fun onFailure(call: Call<ResponseLoanCharge>, t: Throwable) {
                Log.d("loanCharge", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLoanCharge>,
                response: Response<ResponseLoanCharge>
            ) {
                Log.d("loanCharge", Gson().toJson(response.body()))
                Log.d("loanCharge", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("loanCharge", response.code().toString())

                }
            }

        })
    }

    fun loanApply(
        body: BodyLoanApply,
        callBack: (ApiResponse<ResponseLoanApply>) -> Unit
    ) {

        apiServices.loanApply(body).enqueue(object : Callback<ResponseLoanApply> {
            override fun onFailure(call: Call<ResponseLoanApply>, t: Throwable) {
                Log.d("loanApply", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLoanApply>,
                response: Response<ResponseLoanApply>
            ) {
                Log.d("loanApply", Gson().toJson(response.body()))
                Log.d("loanApply", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("loanApply", response.code().toString())

                }
            }

        })
    }

    fun variables(
        body: BodyLang,
        callBack: (ApiResponse<ResponseVariable>) -> Unit
    ) {

        apiServices.variables(body).enqueue(object : Callback<ResponseVariable> {
            override fun onFailure(call: Call<ResponseVariable>, t: Throwable) {
                Log.d("variables", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseVariable>,
                response: Response<ResponseVariable>
            ) {
                Log.d("variables", Gson().toJson(response.body()))
                Log.d("variables", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("variables", response.code().toString())

                }
            }

        })
    }

    fun getQuestions(
        body: BodyLang,
        callBack: (ApiResponse<ResponseQuestions>) -> Unit
    ) {

        apiServices.getQuestions(body).enqueue(object : Callback<ResponseQuestions> {
            override fun onFailure(call: Call<ResponseQuestions>, t: Throwable) {
                Log.d("getQuestions", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseQuestions>,
                response: Response<ResponseQuestions>
            ) {
                Log.d("getQuestions", Gson().toJson(response.body()))
                Log.d("getQuestions", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getQuestions", response.code().toString())

                }
            }

        })
    }

    fun getOffer(
        callBack: (ApiResponse<ResponseOffer>) -> Unit
    ) {

        apiServices.getOffer().enqueue(object : Callback<ResponseOffer> {
            override fun onFailure(call: Call<ResponseOffer>, t: Throwable) {
                Log.d("getOffer", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseOffer>,
                response: Response<ResponseOffer>
            ) {
                Log.d("getOffer", Gson().toJson(response.body()))
                Log.d("getOffer", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getOffer", response.code().toString())

                }
            }

        })
    }

    fun getLoanAccountList(
        callBack: (ApiResponse<ResponseAccountList>) -> Unit
    ) {

        apiServices.getLoanAccountList().enqueue(object : Callback<ResponseAccountList> {
            override fun onFailure(call: Call<ResponseAccountList>, t: Throwable) {
                Log.d("getLoanAccountList", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseAccountList>,
                response: Response<ResponseAccountList>
            ) {
                Log.d("getLoanAccountList", Gson().toJson(response.body()))
                Log.d("getLoanAccountList", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getLoanAccountList", response.code().toString())

                }
            }

        })
    }

    fun getOfferDetails(
        body: BodyOfferDetails,
        callBack: (ApiResponse<ResponseOfferDetails>) -> Unit
    ) {

        apiServices.getOfferDetails(body).enqueue(object : Callback<ResponseOfferDetails> {
            override fun onFailure(call: Call<ResponseOfferDetails>, t: Throwable) {
                Log.d("getOfferDetails", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseOfferDetails>,
                response: Response<ResponseOfferDetails>
            ) {
                Log.d("getOfferDetails", Gson().toJson(response.body()))
                Log.d("getOfferDetails", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getOfferDetails", response.code().toString())

                }
            }

        })
    }

    fun offerClaim(
        body: BodyClaimOffer,
        callBack: (ApiResponse<ResponseClaimOffer>) -> Unit
    ) {

        apiServices.offerClaim(body).enqueue(object : Callback<ResponseClaimOffer> {
            override fun onFailure(call: Call<ResponseClaimOffer>, t: Throwable) {
                Log.d("offerClaim", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseClaimOffer>,
                response: Response<ResponseClaimOffer>
            ) {
                Log.d("offerClaim", Gson().toJson(response.body()))
                Log.d("offerClaim", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("offerClaim", response.code().toString())

                }
            }

        })
    }

    fun getRepayAc(
        callBack: (ApiResponse<ResponseRepayAc>) -> Unit
    ) {

        apiServices.getRepayAc().enqueue(object : Callback<ResponseRepayAc> {
            override fun onFailure(call: Call<ResponseRepayAc>, t: Throwable) {
                Log.d("getRepayAc", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseRepayAc>,
                response: Response<ResponseRepayAc>
            ) {
                Log.d("getRepayAc", Gson().toJson(response.body()))
                Log.d("getRepayAc", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getRepayAc", response.code().toString())

                }
            }

        })
    }

    fun getHishabSummery(
        callBack: (ApiResponse<ResponseHkSummery>) -> Unit
    ) {

        apiServices.getHishabSummery().enqueue(object : Callback<ResponseHkSummery> {
            override fun onFailure(call: Call<ResponseHkSummery>, t: Throwable) {
                Log.d("getHishabSummery", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseHkSummery>,
                response: Response<ResponseHkSummery>
            ) {
                Log.d("getHishabSummery", Gson().toJson(response.body()))
                Log.d("getHishabSummery", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getHishabSummery", response.code().toString())

                }
            }

        })
    }

    fun addCustomer(
        body: BodyHkAddCustomer,
        callBack: (ApiResponse<ResponseHkAddCustomer>) -> Unit
    ) {
        apiServices.addCustomer(body).enqueue(object : Callback<ResponseHkAddCustomer> {
            override fun onFailure(call: Call<ResponseHkAddCustomer>, t: Throwable) {
                Log.d("addCustomer", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseHkAddCustomer>,
                response: Response<ResponseHkAddCustomer>
            ) {
                Log.d("addCustomer", Gson().toJson(response.body()))
                Log.d("addCustomer", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("addCustomer", response.code().toString())

                }
            }

        })
    }

    fun hishabTransaction(
        body: BodyHkTransaction,
        callBack: (ApiResponse<ResponseHkTransaction>) -> Unit
    ) {
        apiServices.hishabTransaction(body).enqueue(object : Callback<ResponseHkTransaction> {
            override fun onFailure(call: Call<ResponseHkTransaction>, t: Throwable) {
                Log.d("hishabTransaction", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseHkTransaction>,
                response: Response<ResponseHkTransaction>
            ) {
                Log.d("hishabTransaction", Gson().toJson(response.body()))
                Log.d("hishabTransaction", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("hishabTransaction", response.code().toString())

                }
            }

        })
    }

    fun hishabDetails(
        body: BodyHkDetails,
        callBack: (ApiResponse<ResponseHkDetails>) -> Unit
    ) {
        apiServices.hishabDetails(body).enqueue(object : Callback<ResponseHkDetails> {
            override fun onFailure(call: Call<ResponseHkDetails>, t: Throwable) {
                Log.d("hishabDetails", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseHkDetails>,
                response: Response<ResponseHkDetails>
            ) {
                Log.d("hishabDetails", Gson().toJson(response.body()))
                Log.d("hishabDetails", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("hishabDetails", response.code().toString())

                }
            }

        })
    }

    fun loanStatusChange(
        body: BodyLoanStatusChange,
        callBack: (ApiResponse<ResponseLoanStatusChange>) -> Unit
    ) {
        apiServices.loanStatusChange(body).enqueue(object : Callback<ResponseLoanStatusChange> {
            override fun onFailure(call: Call<ResponseLoanStatusChange>, t: Throwable) {
                Log.d("loanStatusChange", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLoanStatusChange>,
                response: Response<ResponseLoanStatusChange>
            ) {
                Log.d("loanStatusChange", Gson().toJson(response.body()))
                Log.d("loanStatusChange", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("loanStatusChange", response.code().toString())

                }
            }

        })
    }

    fun addLoanAccount(
        body: BodyLoanAccountAdd,
        callBack: (ApiResponse<ResponseAccountAdd>) -> Unit
    ) {
        apiServices.addLoanAccount(body).enqueue(object : Callback<ResponseAccountAdd> {
            override fun onFailure(call: Call<ResponseAccountAdd>, t: Throwable) {
                Log.d("addLoanAccount", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseAccountAdd>,
                response: Response<ResponseAccountAdd>
            ) {
                Log.d("addLoanAccount", Gson().toJson(response.body()))
                Log.d("addLoanAccount", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("addLoanAccount", response.code().toString())
                }
            }

        })
    }

    fun updateBasic(
        body: BodyUpdateBasic,
        callBack: (ApiResponse<ResponseUpdateBasic>) -> Unit
    ) {
        apiServices.updateBasic(body).enqueue(object : Callback<ResponseUpdateBasic> {
            override fun onFailure(call: Call<ResponseUpdateBasic>, t: Throwable) {
                Log.d("updateBasic", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseUpdateBasic>,
                response: Response<ResponseUpdateBasic>
            ) {
                Log.d("updateBasic", Gson().toJson(response.body()))
                Log.d("updateBasic", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("updateBasic", response.code().toString())
                }
            }

        })
    }

    fun updateProfession(
        body: BodyUpdateProfession,
        callBack: (ApiResponse<ResponseUpdateProfession>) -> Unit
    ) {
        apiServices.updateProfession(body).enqueue(object : Callback<ResponseUpdateProfession> {
            override fun onFailure(call: Call<ResponseUpdateProfession>, t: Throwable) {
                Log.d("updateProfession", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseUpdateProfession>,
                response: Response<ResponseUpdateProfession>
            ) {
                Log.d("updateProfession", Gson().toJson(response.body()))
                Log.d("updateProfession", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("updateProfession", response.code().toString())
                }
            }

        })
    }

    fun repayUploadDoc(
        body: BodyRepayDocUpload,
        callBack: (ApiResponse<ResponseUpdateProfession>) -> Unit
    ) {
        apiServices.repayUploadDoc(body).enqueue(object : Callback<ResponseUpdateProfession> {
            override fun onFailure(call: Call<ResponseUpdateProfession>, t: Throwable) {
                Log.d("repayUploadDoc", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseUpdateProfession>,
                response: Response<ResponseUpdateProfession>
            ) {
                Log.d("repayUploadDoc", Gson().toJson(response.body()))
                Log.d("repayUploadDoc", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("repayUploadDoc", response.code().toString())
                }
            }

        })
    }

    fun updateDoc(
        body: BodyUpdateDoc,
        callBack: (ApiResponse<ResponseUpdateDoc>) -> Unit
    ) {
        apiServices.updateDoc(body).enqueue(object : Callback<ResponseUpdateDoc> {
            override fun onFailure(call: Call<ResponseUpdateDoc>, t: Throwable) {
                Log.d("updateDoc", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseUpdateDoc>,
                response: Response<ResponseUpdateDoc>
            ) {
                Log.d("updateDoc", Gson().toJson(response.body()))
                Log.d("updateDoc", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("updateDoc", response.code().toString())
                }
            }

        })
    }

    fun logMessage(
        callBack: (ApiResponse<ResponseLogMessage>) -> Unit
    ) {
        apiServices.logMessage().enqueue(object : Callback<ResponseLogMessage> {
            override fun onFailure(call: Call<ResponseLogMessage>, t: Throwable) {
                Log.d("logMessage", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLogMessage>,
                response: Response<ResponseLogMessage>
            ) {
                Log.d("logMessage", Gson().toJson(response.body()))
                Log.d("logMessage", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("logMessage", response.code().toString())
                }
            }

        })
    }


    fun logStatements(
        callBack: (ApiResponse<ResponseLogStatement>) -> Unit
    ) {
        apiServices.logStatements().enqueue(object : Callback<ResponseLogStatement> {
            override fun onFailure(call: Call<ResponseLogStatement>, t: Throwable) {
                Log.d("logStatements", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLogStatement>,
                response: Response<ResponseLogStatement>
            ) {
                Log.d("logStatements", Gson().toJson(response.body()))
                Log.d("logStatements", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("logStatements", response.code().toString())
                }
            }

        })
    }


    fun referMessage(
        callBack: (ApiResponse<ResponseReferMessage>) -> Unit
    ) {
        apiServices.referMessage().enqueue(object : Callback<ResponseReferMessage> {
            override fun onFailure(call: Call<ResponseReferMessage>, t: Throwable) {
                Log.d("referMessage", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseReferMessage>,
                response: Response<ResponseReferMessage>
            ) {
                Log.d("referMessage", Gson().toJson(response.body()))
                Log.d("referMessage", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("referMessage", response.code().toString())
                }
            }

        })
    }

    fun loanList(
        callBack: (ApiResponse<ResponseLoanList>) -> Unit
    ) {
        apiServices.loanList().enqueue(object : Callback<ResponseLoanList> {
            override fun onFailure(call: Call<ResponseLoanList>, t: Throwable) {
                Log.d("loanList", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLoanList>,
                response: Response<ResponseLoanList>
            ) {
                Log.d("loanList", Gson().toJson(response.body()))
                Log.d("loanList", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("loanList", response.code().toString())
                }
            }

        })
    }

    fun loanStatus(
        callBack: (ApiResponse<ResponseLoanStatus>) -> Unit
    ) {
        apiServices.loanStatus().enqueue(object : Callback<ResponseLoanStatus> {
            override fun onFailure(call: Call<ResponseLoanStatus>, t: Throwable) {
                Log.d("loanStatus", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLoanStatus>,
                response: Response<ResponseLoanStatus>
            ) {
                Log.d("loanStatus", Gson().toJson(response.body()))
                Log.d("loanStatus", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("loanStatus", response.code().toString())
                }
            }

        })
    }

    fun userProfile(
        callBack: (ApiResponse<ResponseUserProfile>) -> Unit
    ) {
        apiServices.userProfile().enqueue(object : Callback<ResponseUserProfile> {
            override fun onFailure(call: Call<ResponseUserProfile>, t: Throwable) {
                Log.d("userProfile", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseUserProfile>,
                response: Response<ResponseUserProfile>
            ) {
                Log.d("userProfile", Gson().toJson(response.body()))
                Log.d("userProfile", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("userProfile", response.code().toString())
                }
            }

        })
    }

    fun userStatus(
        callBack: (ApiResponse<ResponseUserStatus>) -> Unit
    ) {
        apiServices.userStatus().enqueue(object : Callback<ResponseUserStatus> {
            override fun onFailure(call: Call<ResponseUserStatus>, t: Throwable) {
                Log.d("userStatus", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseUserStatus>,
                response: Response<ResponseUserStatus>
            ) {
                Log.d("userStatus", Gson().toJson(response.body()))
                Log.d("userStatus", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("userStatus", response.code().toString())
                }
            }

        })
    }

    fun bankList(
        callBack: (ApiResponse<ResponseBankList>) -> Unit
    ) {
        apiServices.bankList().enqueue(object : Callback<ResponseBankList> {
            override fun onFailure(call: Call<ResponseBankList>, t: Throwable) {
                Log.d("bankList", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseBankList>,
                response: Response<ResponseBankList>
            ) {
                Log.d("bankList", Gson().toJson(response.body()))
                Log.d("bankList", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("bankList", response.code().toString())
                }
            }

        })
    }

    fun getBranch(
        bankId: String,
        callBack: (ApiResponse<ResponseBranch>) -> Unit
    ) {
        apiServices.getBranch(bankId).enqueue(object : Callback<ResponseBranch> {
            override fun onFailure(call: Call<ResponseBranch>, t: Throwable) {
                Log.d("getBranch", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseBranch>,
                response: Response<ResponseBranch>
            ) {
                Log.d("getBranch", Gson().toJson(response.body()))
                Log.d("getBranch", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("getBranch", response.code().toString())
                }
            }

        })
    }


    //=========================================================================================================

    fun nidOcr(
        body: BodyOcr,
        callBack: (ApiResponse<ResponseOcr>) -> Unit
    ) {
        ocrApiServices.nidOcr(body).enqueue(object : Callback<ResponseOcr> {
            override fun onFailure(call: Call<ResponseOcr>, t: Throwable) {
                Log.d("nidOcr", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseOcr>,
                response: Response<ResponseOcr>
            ) {
                Log.d("nidOcr", Gson().toJson(response.body()))
                Log.d("nidOcr", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("nidOcr", response.code().toString())

                }
            }

        })
    }

    fun imageMatching(
        body: BodyImageMatching,
        callBack: (ApiResponse<ResponseImageMatching>) -> Unit
    ) {
        imageMatchApiServices.match(body).enqueue(object : Callback<ResponseImageMatching> {
            override fun onFailure(call: Call<ResponseImageMatching>, t: Throwable) {
                Log.d("imageMatching", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseImageMatching>,
                response: Response<ResponseImageMatching>
            ) {
                Log.d("imageMatching", Gson().toJson(response.body()))
                Log.d("imageMatching", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("imageMatching", response.code().toString())

                }
            }

        })
    }

    fun checkLiveness(
        body: ArrayList<BodyLiveness.BodyLivenessItem>,
        callBack: (ApiResponse<ResponseLiveness>) -> Unit
    ) {
        livenessApiServices.inference(body).enqueue(object : Callback<ResponseLiveness> {
            override fun onFailure(call: Call<ResponseLiveness>, t: Throwable) {
                Log.d("checkLiveness", t.localizedMessage)
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseLiveness>,
                response: Response<ResponseLiveness>
            ) {
                Log.d("checkLiveness", Gson().toJson(response.body()))
                Log.d("checkLiveness", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("checkLiveness", response.code().toString())

                }
            }

        })
    }

    fun smsParsing(
        body: BodySmsParsing,
        callBack: (ApiResponse<ResponseSmsParsing>) -> Unit
    ) {
        val requestBody: RequestBody = RequestBody.create(
            MediaType.parse("text/plain"),
            Gson().toJson(body.data)
        )

        deviceDataApiServices.smsParsing(requestBody).enqueue(object : Callback<ResponseSmsParsing> {
            override fun onFailure(call: Call<ResponseSmsParsing>, t: Throwable) {
                Log.d("smsParsing", t.localizedMessage ?: "error")
                callBack(ApiResponse(code = -1, message = ON_FAILURE_MESSAGE))
            }

            override fun onResponse(
                call: Call<ResponseSmsParsing>,
                response: Response<ResponseSmsParsing>
            ) {
                Log.d("smsParsing", Gson().toJson(response.body()))
                Log.d("smsParsing", response.code().toString())

                if (response.isSuccessful) {
                    callBack(ApiResponse(code = response.code(), data = response.body()))
                } else {
                    callBack(
                        ApiResponse(
                            code = response.code(),
                            message = "Connection Timeout ${response.code()}"
                        )
                    )
                    Log.d("smsParsing", response.code().toString())

                }
            }

        })
    }


}