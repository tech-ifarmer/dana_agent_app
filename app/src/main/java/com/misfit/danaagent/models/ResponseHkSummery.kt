package com.misfit.dana.models

data class ResponseHkSummery(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val customers: ArrayList<Customer>,
            val total: String?
        ) {
            data class Customer(
                val id: String?,
                val balance: String?,
                val name: String?,
                val updated_at: String?
            )
        }
    }
}