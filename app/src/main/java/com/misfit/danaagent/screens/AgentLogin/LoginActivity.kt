package com.misfit.danaagent.screens.AgentLogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        btn_proceed.setOnClickListener {
            startActivity(
                Intent(applicationContext, AgentOTPActivity::class.java)
            )
        }
    }
}