package com.misfit.danaagent.screens.Information

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.misfit.dana.data.RuntimeData
import com.misfit.dana.models.BodyUpdateProfession
import com.misfit.dana.utils.AppUtils
import com.misfit.dana.utils.DialogUtil
import com.misfit.dana.utils.hide
import com.misfit.dana.utils.show
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.IncomeExpanse.IncomeExpenceInfoActivity
import com.parallax.showofff.utils.BASE64_PREFIX
import com.parallax.showofff.utils.ProfessionType
import kotlinx.android.synthetic.main.activity_information.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.ByteArrayOutputStream

class InformationActivity : AppCompatActivity() {

    var imagePickFor = -1

    val IMAGE_BUSINESS_TRADE_LICENSE = 1
    val IMAGE_EMPLOYEE_PAY_SLIP = 2
    val IMAGE_EMPLOYEE_INCOME_DOCUMENT = 3
    val IMAGE_EMPLOYEE_ID_CARD = 4
    val IMAGE_STUDENT_ID_CARD = 5

    val easyImage = EasyImage.Builder(this).build()

    var bodyUpdateProfession = BodyUpdateProfession()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)

        rv_edu.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_edu.adapter = SingleSelectAdapter(applicationContext) {
            RuntimeData.bodyUpdateBasic?.edu_level = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.edu_level?.let { list ->
                resetItems(list)
            }
        }

        // Business

        rv_business_location.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_business_location.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.location = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.business_location?.let { list ->
                resetItems(list)
            }
        }
        rv_business_type.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_business_type.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.type = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.type?.let { list ->
                resetItems(list)
            }
        }
        rv_business_duration.layoutManager = GridLayoutManager(applicationContext, 3)
        rv_business_duration.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.duration = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.duration?.let { list ->
                resetItems(list)
            }
        }
        rv_business_nature.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_business_nature.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.nature = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.nature?.let { list ->
                resetItems(list)
            }
        }
        rv_business_prev.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_business_prev.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.prev_business = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.prev_business?.let { list ->
                resetItems(list)
            }
        }

        et_business_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                bodyUpdateProfession.distributor = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        iv_business_trade_license.setOnClickListener {
            imagePickFor = IMAGE_BUSINESS_TRADE_LICENSE
            AppUtils.showImageChooseDialog(this) {
                when (it) {
                    0 -> {
                        easyImage.openCameraForImage(this)
                    }
                    1 -> {
                        easyImage.openGallery(this)
                    }
                }
            }
        }

        // Employee

        rv_company_location.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_company_location.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.location = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.employee_location?.let { list ->
                resetItems(list)
            }
        }
        rv_emp_type.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_emp_type.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.emp_type = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.emp_type?.let { list ->
                resetItems(list)
            }
        }

        if (RuntimeData.nidOcr?.voter?.spouse.isNullOrBlank()) {
            layout_spouse_income.hide()
        } else {
            layout_spouse_income.show()
            rv_emp_spouse_income.layoutManager = GridLayoutManager(applicationContext, 2)
            rv_emp_spouse_income.adapter = SingleSelectAdapter(applicationContext) {
                bodyUpdateProfession.spouse_income = it
            }.apply {
                RuntimeData.responseVariable?.response?.data?.data?.spouse_income?.let { list ->
                    resetItems(list)
                }
            }
        }


        rv_emp_duration.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_emp_duration.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.duration = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.duration?.let { list ->
                resetItems(list)
            }
        }
        rv_emp_income_document.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_emp_income_document.adapter = SingleSelectAdapter(applicationContext) {
            when(it){
                "Yes" -> layout_pay_slip.show()
                "No" -> layout_pay_slip.hide()
            }
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.has_invoice?.let { list ->
                resetItems(list)
            }
        }

        et_company_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                bodyUpdateProfession.company = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

//        et_designation.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                bodyUpdateProfession.designation = s.toString()
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
//            }
//        })

        iv_employee_pay_slip.setOnClickListener {
            imagePickFor = IMAGE_EMPLOYEE_PAY_SLIP
            AppUtils.showImageChooseDialog(this) {
                when (it) {
                    0 -> {
                        easyImage.openCameraForImage(this)
                    }
                    1 -> {
                        easyImage.openGallery(this)
                    }
                }
            }
        }
        iv_employee_income_document.setOnClickListener {
            imagePickFor = IMAGE_EMPLOYEE_INCOME_DOCUMENT
            AppUtils.showImageChooseDialog(this) {
                when (it) {
                    0 -> {
                        easyImage.openCameraForImage(this)
                    }
                    1 -> {
                        easyImage.openGallery(this)
                    }
                }
            }
        }
        iv_employee_id_card.setOnClickListener {
            imagePickFor = IMAGE_EMPLOYEE_ID_CARD
            AppUtils.showImageChooseDialog(this) {
                when (it) {
                    0 -> {
                        easyImage.openCameraForImage(this)
                    }
                    1 -> {
                        easyImage.openGallery(this)
                    }
                }
            }
        }

        // Farmer

        rv_farm_location.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_farm_location.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.location = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.farmer_location?.let { list ->
                resetItems(list)
            }
        }
        rv_farm_type.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_farm_type.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.farm_type = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.farm_type?.let { list ->
                resetItems(list)
            }
        }
        rv_farm_experience.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_farm_experience.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.exp = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.exp?.let { list ->
                resetItems(list)
            }
        }
        rv_farm_size.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_farm_size.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.farm_size = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.farm_size?.let { list ->
                resetItems(list)
            }
        }
        rv_farm_ownership.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_farm_ownership.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.farm_ownership = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.farm_ownership?.let { list ->
                resetItems(list)
            }
        }

        // Student

        rv_student_faculty.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_faculty.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.faculty = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.faculty?.let { list ->
                resetItems(list)
            }
        }

        rv_student_gpa.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_gpa.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.gpa = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.gpa?.let { list ->
                resetItems(list)
            }
        }

        rv_student_duration.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_duration.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.duration = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.duration?.let { list ->
                resetItems(list)
            }
        }

        rv_student_school_type.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_school_type.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.school_type = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.school_type?.let { list ->
                resetItems(list)
            }
        }

        rv_student_family_income.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_family_income.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.family_income = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.family_income?.let { list ->
                resetItems(list)
            }
        }

        rv_student_father_edu.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_father_edu.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.father_edu = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.father_edu?.let { list ->
                resetItems(list)
            }
        }

        rv_student_mother_edu.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_mother_edu.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.mother_edu = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.mother_edu?.let { list ->
                resetItems(list)
            }
        }

        rv_student_donor.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_student_donor.adapter = SingleSelectAdapter(applicationContext) {
            bodyUpdateProfession.donor = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.donor?.let { list ->
                resetItems(list)
            }
        }

//        et_student_id.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
////                bodyUpdateProfession.id_card = s.toString()
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
//            }
//        })

        iv_student_id.setOnClickListener {
            imagePickFor = IMAGE_STUDENT_ID_CARD
            AppUtils.showImageChooseDialog(this) {
                when (it) {
                    0 -> {
                        easyImage.openCameraForImage(this)
                    }
                    1 -> {
                        easyImage.openGallery(this)
                    }
                }
            }
        }



        group_profession.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.btn_bussinuss -> {
                    layout_business.visibility = View.VISIBLE
                    layout_salaried.visibility = View.GONE
                    layout_farmer.visibility = View.GONE
                    layout_student.visibility = View.GONE

                    bodyUpdateProfession.prof_type = ProfessionType.Business.name
                    RuntimeData.bodyUpdateBasic?.prof_type = ProfessionType.Business.name
                }
                R.id.btn_salaried -> {
                    layout_business.visibility = View.GONE
                    layout_salaried.visibility = View.VISIBLE
                    layout_farmer.visibility = View.GONE
                    layout_student.visibility = View.GONE

                    bodyUpdateProfession.prof_type = ProfessionType.Employee.name
                    RuntimeData.bodyUpdateBasic?.prof_type = ProfessionType.Employee.name
                }
                R.id.btn_farmer -> {
                    layout_business.visibility = View.GONE
                    layout_salaried.visibility = View.GONE
                    layout_farmer.visibility = View.VISIBLE
                    layout_student.visibility = View.GONE

                    bodyUpdateProfession.prof_type = ProfessionType.Farmer.name
                    RuntimeData.bodyUpdateBasic?.prof_type = ProfessionType.Farmer.name
                }
                R.id.btn_student -> {
                    layout_business.visibility = View.GONE
                    layout_salaried.visibility = View.GONE
                    layout_farmer.visibility = View.GONE
                    layout_student.visibility = View.VISIBLE

                    bodyUpdateProfession.prof_type = ProfessionType.Student.name
                    RuntimeData.bodyUpdateBasic?.prof_type = ProfessionType.Student.name
                }
                else -> {
                    layout_business.visibility = View.GONE
                    layout_salaried.visibility = View.GONE
                }
            }
        }

        btn_next.setOnClickListener {

            startActivity(Intent(applicationContext, IncomeExpenceInfoActivity::class.java))

        }

    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : DefaultCallback() {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {
                    val bm = BitmapFactory.decodeFile(imageFiles[0].file.path)
                    val baos = ByteArrayOutputStream()
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) // bm is the bitmap object

                    val b: ByteArray = baos.toByteArray()

                    val encodedImage: String = Base64.encodeToString(b, Base64.DEFAULT)

                    Log.d("image_base64", encodedImage)

                    when (imagePickFor) {
                        IMAGE_BUSINESS_TRADE_LICENSE -> {
                            Glide.with(this@InformationActivity).load(b)
                                .into(iv_business_trade_license)

                            bodyUpdateProfession.trade_lic = BASE64_PREFIX + encodedImage
                        }
                        IMAGE_EMPLOYEE_ID_CARD -> {
                            Glide.with(this@InformationActivity).load(b)
                                .into(iv_employee_id_card)

                            bodyUpdateProfession.emp_id = BASE64_PREFIX + encodedImage
                        }
                        IMAGE_EMPLOYEE_INCOME_DOCUMENT -> {
                            Glide.with(this@InformationActivity).load(b)
                                .into(iv_employee_income_document)

                            bodyUpdateProfession.income_doc = BASE64_PREFIX + encodedImage
                        }
                        IMAGE_EMPLOYEE_PAY_SLIP -> {
                            Glide.with(this@InformationActivity).load(b)
                                .into(iv_employee_pay_slip)

                            bodyUpdateProfession.invoice = BASE64_PREFIX + encodedImage
                        }
                        IMAGE_STUDENT_ID_CARD -> {
                            Glide.with(this@InformationActivity).load(b)
                                .into(iv_student_id)

                            bodyUpdateProfession.id_card = BASE64_PREFIX + encodedImage
                        }
                    }

                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                    //Some error handling
                    error.printStackTrace()
                }

                override fun onCanceled(source: MediaSource) {
                    //Not necessary to remove any files manually anymore
                }
            })
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        AppUtils.hideKeyboard(this)
        return super.dispatchTouchEvent(ev)
    }



}