package com.misfit.dana.utils

import android.app.Activity
import android.app.Dialog
import com.misfit.danaagent.R
import kotlinx.android.synthetic.main.dialog_confirmation.*
import kotlinx.android.synthetic.main.dialog_confirmation.textView7
import kotlinx.android.synthetic.main.dialog_message.*
import kotlinx.android.synthetic.main.dialog_message.btn_edit

object DialogUtil {

    fun showConfirmationDialog(activity: Activity, message: String, onPositiveClick: () -> Unit) {
        val dialog = Dialog(activity)
        dialog.setContentView(R.layout.dialog_confirmation)
        dialog.setCancelable(false)
        dialog.textView7.text = message
        dialog.btn_edit.setOnClickListener {
            dialog.dismiss()
        }
        dialog.btn_yes.setOnClickListener {
            dialog.dismiss()
            onPositiveClick()
        }
        dialog.show()
    }

    fun showMessageDialog(activity: Activity, message: String, onOkClick: () -> Unit) {
        val dialog = Dialog(activity)
        dialog.setContentView(R.layout.dialog_message)
        dialog.setCancelable(false)
        dialog.tv_message.text = message
        dialog.btn_ok.setOnClickListener {
            dialog.dismiss()
            onOkClick()
        }
        dialog.show()
    }

}