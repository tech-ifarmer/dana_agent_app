package com.misfit.dana.models

data class ResponseBankList(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val banks: List<Bank>
        ) {
            data class Bank(
                val code: String?,
                val id: String,
                val name: String?
            )
        }
    }
}