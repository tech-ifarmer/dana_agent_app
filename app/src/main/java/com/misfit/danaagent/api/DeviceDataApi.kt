package com.misfit.dana.api

import com.misfit.dana.models.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface DeviceDataApi {
    @Multipart
    @POST("sms-parsing/")
    fun smsParsing(@Part("data") data : RequestBody): Call<ResponseSmsParsing>
}