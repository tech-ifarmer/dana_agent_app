package com.parallax.showofff.api

class ApiResponse<T>(
    var code: Int,
    var message: String? = null,
    var data: T? = null
)