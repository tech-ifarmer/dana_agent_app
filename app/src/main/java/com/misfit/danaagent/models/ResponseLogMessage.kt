package com.misfit.dana.models

data class ResponseLogMessage(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val messages: ArrayList<Message>
        ) {
            data class Message(
                val date: String?,
                val details: String?,
                val title: String?
            )
        }
    }
}