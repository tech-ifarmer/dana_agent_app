package com.misfit.dana.api

import com.misfit.dana.models.BodyOcr
import com.misfit.dana.models.ResponseOcr
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface OcrApi {
    @POST("nid-ocr")
    fun nidOcr(@Body body: BodyOcr): Call<ResponseOcr>
}