package com.misfit.danaagent.screens.CameraNIDFront

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.CameraNIDBack.NidBackCamera
import kotlinx.android.synthetic.main.activity_nid_front_image_check.*
import kotlinx.android.synthetic.main.activity_nid_front_image_check.btn_back

class NidFrontImageCheckActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nid_front_image_check)

        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            val message = bundle.getByteArray("front")
            Glide.with(this).load(message).into(iv)
        }




        btn_next.setOnClickListener {
            startActivity(Intent(applicationContext, NidBackCamera::class.java))
        }

        btn_again.setOnClickListener {
            onBackPressed()
        }

        btn_back.setOnClickListener { onBackPressed() }
    }
}