package com.misfit.dana.models

data class ResponseLoanCharge(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val amount: Double?,
            val repayment: Double?,
            val repayment_date: String?,
            val service_charge_percentage: String?,
            val service_charge: Double?
        )
    }
}