package com.misfit.dana.models

data class ResponseLogin(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val person_id: String?,
            val token: String?,
            val username: Any?
        )
    }
}