package com.misfit.dana.models

data class ResponseVariable(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val `data`: Data?,
            val status: Int?
        ) {
            data class Data(
                val age: ArrayList<String>,
                val brand: ArrayList<String>,
                val business_location: ArrayList<String>,
                val call_in: ArrayList<String>,
                val call_out: ArrayList<String>,
                val csr: ArrayList<String>,
                val cur_duration: ArrayList<String>,
                val cur_exp: ArrayList<String>,
                val donor: ArrayList<String>,
                val duration: ArrayList<String>,
                val earning_member: ArrayList<String>,
                val edu_level: ArrayList<String>,
                val emp_type: ArrayList<String>,
                val employee_location: ArrayList<String>,
                val exp: ArrayList<String>,
                val faculty: ArrayList<String>,
                val fam_mem: ArrayList<String>,
                val family_income: ArrayList<String>,
                val farm_size: ArrayList<String>,
                val farm_type: ArrayList<String>,
                val farmer_location: ArrayList<String>,
                val father_edu: ArrayList<String>,
                val gender: ArrayList<String>,
                val gpa: ArrayList<String>,
                val has_bank_ac: ArrayList<String>,
                val has_bank_statement: ArrayList<String>,
                val has_extra_income: ArrayList<String>,
                val has_id_card: ArrayList<String>,
                val has_income_doc: ArrayList<String>,
                val has_invoice: ArrayList<String>,
                val has_membership: ArrayList<String>,
                val has_prev_loan: ArrayList<String>,
                val has_savings: ArrayList<String>,
                val has_tax_doc: ArrayList<String>,
                val has_utility_bill: ArrayList<String>,
                val home_ownership: ArrayList<String>,
                val home_type: ArrayList<String>,
                val income: ArrayList<String>,
                val internet: ArrayList<String>,
                val lifetime: ArrayList<String>,
                val married: ArrayList<String>,
                val mother_edu: ArrayList<String>,
                val nature: ArrayList<String>,
                val no_loan: ArrayList<String>,
                val no_missed_call: ArrayList<String>,
                val prev_business: ArrayList<String>,
                val prof_type: ArrayList<String>,
                val purpose: ArrayList<String>,
                val recharge: ArrayList<String>,
                val repayment: ArrayList<String>,
                val residence_location: ArrayList<String>,
                val school_type: ArrayList<String>,
                val sms_in: ArrayList<String>,
                val sms_out: ArrayList<String>,
                val spouse_income: ArrayList<String>,
                val talktime: ArrayList<String>,
                val expense: ArrayList<String>,
                val type: ArrayList<String>,
                val farm_ownership : ArrayList<String>,
                val years_residence: ArrayList<String>
            )
        }
    }
}