package com.dana.money.screens.psychomethic_question.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.misfit.dana.models.ResponseQuestions
import com.misfit.dana.utils.hide
import com.misfit.dana.utils.show
import com.misfit.danaagent.R
import com.parallax.showofff.utils.IMAGE_BASE_URL
import kotlinx.android.synthetic.main.item_psychometric_ques_with_image.view.*

class WithImageAdapter(val context: Context, val onItemClick: (ResponseQuestions.Response.Data.Question.An) -> Unit) :
    RecyclerView.Adapter<WithImageAdapter.CustomView>() {

    val items = arrayListOf<ResponseQuestions.Response.Data.Question.An>()

    var selectedView: RelativeLayout? = null
    var selectedImageView: ImageView? = null

    fun resetItems(newItems: ArrayList<ResponseQuestions.Response.Data.Question.An>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        return CustomView(
            LayoutInflater.from(context).inflate(R.layout.item_psychometric_ques_with_image, null, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {

        Glide.with(context).load(IMAGE_BASE_URL + items[position].ans).into(holder.itemView.iv)

        holder.itemView.tv_ques.text = items[position].comment

        holder.itemView.layout_parent.setOnClickListener {

            onItemClick(items[position])

            if (selectedView == null) {
                holder.itemView.layout_parent.setBackgroundResource(R.drawable.bg_green_border)
                holder.itemView.iv_check.show()

                selectedView = holder.itemView.layout_parent
                selectedImageView = holder.itemView.iv_check
            } else {

                selectedView?.setBackgroundResource(R.drawable.bg_gray_border)
                selectedImageView?.hide()

                holder.itemView.layout_parent.setBackgroundResource(R.drawable.bg_green_border)
                holder.itemView.iv_check.show()

                selectedView = holder.itemView.layout_parent
                selectedImageView = holder.itemView.iv_check
//                }


            }
        }


    }


    class CustomView(var view: View) : RecyclerView.ViewHolder(view)
}