package com.misfit.dana.models

data class ResponseSendOtp(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val otp_id: Int?,
            val otp_send: Boolean?
        )
    }
}