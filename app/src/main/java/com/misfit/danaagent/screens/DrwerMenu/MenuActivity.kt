package com.misfit.danaagent.screens.DrwerMenu

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.ApplicationListActivity
import com.misfit.danaagent.screens.BlankActivity
import com.misfit.danaagent.screens.EditProfile.EditProfileActivity
import com.misfit.danaagent.screens.userList.UserListActivity
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.fragment_home.*

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val intent = Intent(applicationContext, BlankActivity::class.java )


        btn_edit_profile.setOnClickListener {
            startActivity(Intent(applicationContext, EditProfileActivity::class.java))
        }



        btn_user_list.setOnClickListener {
            startActivity(Intent(applicationContext, UserListActivity::class.java))
        }

        settings.setOnClickListener {
            intent.putExtra("title", "Settings")
            startActivity(intent)
        }

        btn_about_dana.setOnClickListener {
            intent.putExtra("title", "About")
            startActivity(intent)
        }

        policy.setOnClickListener {
            intent.putExtra("title", "Policy")
            startActivity(intent)
        }

        help.setOnClickListener {
            intent.putExtra("title", "Help")
            startActivity(intent)
        }




    }
}