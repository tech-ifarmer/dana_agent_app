package com.misfit.danaagent.screens

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.PermissionsActivity.PermissionsActivity
import com.misfit.danaagent.utils.viewHelper
import com.misfit.danaagent.utils.viewHelper.White_transparentStatusBar
import com.misfit.danaagent.utils.viewHelper.setLightStatusBar
import com.misfit.danaagent.utils.viewHelper.transparentStatusBar
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLightStatusBar(getWindow().getDecorView().findViewById(android.R.id.content), this);
        setContentView(R.layout.activity_welcome)

        btn_agree.setOnClickListener {
            startActivity(Intent(applicationContext, PermissionsActivity::class.java))
        }
    }
}