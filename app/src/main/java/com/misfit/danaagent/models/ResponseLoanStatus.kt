package com.misfit.dana.models

data class ResponseLoanStatus(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val loan: Loan?,
            val comment : String?,
            val loan_status: String?,
            val unpaid: String?
        ) {
            data class Loan(
                val ac_no: String?,
                val ac_type: String?,
                val amount: String?,
                val approved: String?,
                val bank: String?,
                val branch: String?,
                val loan_id: String?,
                val repayment: Int?,
                val repayment_date: String?,
                val service_charge: Double?,
                val trxn_id: String?
            )
        }
    }
}