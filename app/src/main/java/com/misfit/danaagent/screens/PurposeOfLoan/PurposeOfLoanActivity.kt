package com.misfit.danaagent.screens.PurposeOfLoan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.misfit.dana.data.RuntimeData
import com.misfit.dana.utils.AppUtils
import com.misfit.dana.utils.DialogUtil
import com.misfit.dana.utils.hide
import com.misfit.dana.utils.show
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.psychomethic_question.PsychometricQuestionActivity
import kotlinx.android.synthetic.main.activity_purpose_of_loan.*

class PurposeOfLoanActivity : AppCompatActivity() {


//    var purpose = RuntimeData.responseVariable?.response?.data?.data?.purpose
    lateinit var loanPurposeAdapter: LoanPurposeAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purpose_of_loan)


        et_other_loan_purpose.hide()

        loanPurposeAdapter = LoanPurposeAdapter(applicationContext) {
            RuntimeData.loanApplyModel?.purpose = it

            if (it == "Others") {
                et_other_loan_purpose.show()
            } else {
                et_other_loan_purpose.hide()
            }
        }

        rv.layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        rv.adapter = loanPurposeAdapter

        val list: ArrayList<String> = ArrayList()
        list.add("To pay bill")
        list.add("To buy a gadget")
        list.add("To to travel")



        loanPurposeAdapter.resetItems(list!!)

        btn_next.setOnClickListener {
            startActivity(
                Intent(
                    applicationContext,
                    PsychometricQuestionActivity::class.java
                )
            )

        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        AppUtils.hideKeyboard(this)
        return super.dispatchTouchEvent(ev)
    }
}