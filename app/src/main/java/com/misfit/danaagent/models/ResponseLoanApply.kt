package com.misfit.dana.models

data class ResponseLoanApply(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val loan_id: Int?,
            val msg: String?
        )
    }
}