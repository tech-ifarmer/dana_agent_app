package com.misfit.danaagent.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.adapter.DeclinedListAdapter
import com.misfit.danaagent.adapter.SampleAdapter
import kotlinx.android.synthetic.main.activity_application_list.*
import kotlinx.android.synthetic.main.activity_blank.*

class BlankActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blank)

        val bundle: Bundle? = intent.extras
        if (bundle != null)
            txt.text = bundle.getString("title")



    }



}