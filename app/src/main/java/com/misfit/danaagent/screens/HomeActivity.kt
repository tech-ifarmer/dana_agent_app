package com.misfit.danaagent.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.misfit.dana.utils.hide
import com.misfit.dana.utils.show
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, HomeFragment()).commit()
        activeHome()
        initClick();
    }


    fun initClick() {
        tab_home.setOnClickListener {
            activeHome()
        }

    }

    private fun activeHome() {
        view_home.show()
        view_message.hide()
        view_chat.hide()
        view_people.hide()

        iv_tab_home.setImageResource(R.drawable.img_tab_home)
        iv_tab_message.setImageResource(R.drawable.img_tab_message_uncheck)
        iv_tab_chat.setImageResource(R.drawable.img_tab_chat_uncheck)
        iv_tab_people.setImageResource(R.drawable.img_tab_people_uncheck)

        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, HomeFragment())
            .commit()
    }


//    private fun activePeople() {
//        view_home.hide()
//        view_message.hide()
//        view_chat.hide()
//        view_people.show()
//
//        iv_tab_home.setImageResource(R.drawable.img_tab_home_uncheck)
//        iv_tab_message.setImageResource(R.drawable.img_tab_message_uncheck)
//        iv_tab_chat.setImageResource(R.drawable.img_tab_chat_uncheck)
//        iv_tab_people.setImageResource(R.drawable.img_tab_people)
//
//        supportFragmentManager.beginTransaction()
//            .replace(R.id.fragment_container, ProfileFragment())
//            .commit()
//    }
//
//    private fun activeChat() {
//        view_home.hide()
//        view_message.hide()
//        view_chat.show()
//        view_people.hide()
//
//        iv_tab_home.setImageResource(R.drawable.img_tab_home_uncheck)
//        iv_tab_message.setImageResource(R.drawable.img_tab_message_uncheck)
//        iv_tab_chat.setImageResource(R.drawable.img_tab_chat)
//        iv_tab_people.setImageResource(R.drawable.img_tab_people_uncheck)
//
//
//        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, ChatFragment())
//            .commit()
//    }
//
//    fun activeMessage() {
//        view_home.hide()
//        view_message.show()
//        view_chat.hide()
//        view_people.hide()
//
//        iv_tab_home.setImageResource(R.drawable.img_tab_home_uncheck)
//        iv_tab_message.setImageResource(R.drawable.img_tab_message)
//        iv_tab_chat.setImageResource(R.drawable.img_tab_chat_uncheck)
//        iv_tab_people.setImageResource(R.drawable.img_tab_people_uncheck)
//
//        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, InboxFragment())
//            .commit()
//    }


}