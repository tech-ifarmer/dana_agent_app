package com.misfit.danaagent.screens.Verification

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class PhoneNumberActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_number)

        btn_proceed.setOnClickListener {
            startActivity(
                Intent(applicationContext, OTPActivity::class.java)
            )
        }


    }
}