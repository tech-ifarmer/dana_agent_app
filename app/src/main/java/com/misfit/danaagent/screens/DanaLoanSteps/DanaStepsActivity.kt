package com.misfit.danaagent.screens.DanaLoanSteps

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.CameraNIDFront.NidFrontCamera
import kotlinx.android.synthetic.main.activity_dana_steps.*

class DanaStepsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dana_steps)

        btn_agree.setOnClickListener {
            startActivity(Intent(applicationContext, NidFrontCamera::class.java))
        }

        btn_back.setOnClickListener { onBackPressed() }

    }
}