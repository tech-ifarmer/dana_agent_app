package com.misfit.dana.models

data class ResponseLiveness(
    val error: String?,
    val result: Result?,
    val success: Boolean?
) {
    data class Result(
        val is_attack: Boolean?,
        val results: List<Result?>?,
        val score: Double?
    ) {
        data class Result(
            val error: String?,
            val id: String?,
            val is_attack: Boolean?,
            val is_error: Boolean?,
            val score: Double?
        )
    }
}