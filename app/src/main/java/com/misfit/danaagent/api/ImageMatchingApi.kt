package com.misfit.dana.api

import com.misfit.dana.models.BodyImageMatching
import com.misfit.dana.models.BodySendOtp
import com.misfit.dana.models.ResponseImageMatching
import com.misfit.dana.models.ResponseLogin
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ImageMatchingApi {
    @POST("match")
    fun match(@Body body: BodyImageMatching): Call<ResponseImageMatching>
}