package com.misfit.dana.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.misfit.dana.Dana

object SPreferences {
    private val SP_USER_OBJ = "loginObj"
    private val SP_USER_TOKEN = "loginToken"
    private val SP_PHONE_NO = "phone_no"

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setUserId(loginObj: String?) {
        getSharedPreferences(Dana.instance).edit().putString(SP_USER_OBJ, loginObj).apply()
    }

    fun getUserId(): String? {
        return getSharedPreferences(Dana.instance).getString(SP_USER_OBJ, null)
    }

    fun setUserToken(loginToken: String?) {
        getSharedPreferences(Dana.instance).edit().putString(SP_USER_TOKEN, loginToken).apply()
    }

    fun getUserToken(): String? {
        return getSharedPreferences(Dana.instance).getString(SP_USER_TOKEN, null)
    }

    fun setPhoneNo(data: String?) {
        getSharedPreferences(Dana.instance).edit().putString(SP_PHONE_NO, data).apply()
    }

    fun getPhoneNo(): String? {
        return getSharedPreferences(Dana.instance).getString(SP_PHONE_NO, null)
    }
}