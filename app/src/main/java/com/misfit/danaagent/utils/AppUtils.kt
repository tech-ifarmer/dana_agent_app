package com.misfit.dana.utils

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

object AppUtils {

    lateinit var progressDialog: ProgressDialog

    fun showKeyboard(activity: Activity, editText: EditText) {
        try {
            editText.requestFocus()
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        } catch (e: Exception) {

        }
    }

    fun hideKeyboard(activity: Activity) {
        val view = activity.findViewById<View>(android.R.id.content)
        try {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {

        }
    }


    fun getMoneyFormat(number: String?): String? {
        if (number == null || number.isEmpty()) return ""
        val decimalFormat = DecimalFormat("#,###.#")
        return decimalFormat.format(number.toDouble())
    }


    fun showImageChooseDialog(activity: Activity, onChoose: (Int) -> Unit) {
        val colors = arrayOf(
            "Camera", "Gallery"
        )

        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setTitle("Please choose an option")
        builder.setItems(colors) { dialog, which ->
            onChoose(which)
        }
        builder.show()
    }

    fun getDaysDifference(fromDate: Date?, toDate: Date?): Long {
        return if (fromDate == null || toDate == null) 0 else ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24))
    }


    fun showLoading(activity: Activity, message: String = "Loading..") {
        progressDialog = ProgressDialog(activity)
        progressDialog.setMessage(message)
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    fun hideLoading() {
        if (progressDialog != null && progressDialog?.isShowing) {
            progressDialog?.dismiss()
        }
    }

    fun getNumberFormat(number: String?): String? {
        if (number == null || number.isEmpty()) return ""
        val decimalFormat = DecimalFormat("#.#")
        return decimalFormat.format(number.toDouble())
    }

    fun getNumberFormat(number: Double): String? {
        val decimalFormat = DecimalFormat("#.#")
        return decimalFormat.format(number)
    }

    fun getDateFormat(date: String): String {
        var date =
            SimpleDateFormat("yyyy-MM-dd").parse(date)
        var a = SimpleDateFormat("yyyy-MM-dd").format(date)
        return a
    }


    fun repayDate(duration: Int) : String{


        var dt = Date()
        val c = Calendar.getInstance()
        c.time = dt
        c.add(Calendar.DATE, duration)
        dt = c.time


//        val c = Calendar.getInstance()
//        val year = c.get(Calendar.YEAR)
//        val month = c.get(Calendar.MONTH)+1
//        val day = c.get(Calendar.DAY_OF_MONTH) + duration
//
//        val date = ""+ day + "-" + month + "-" + year
//
        var a =  SimpleDateFormat("dd-MM-yyyy").format(dt)
//
        return a

    }


}