package com.misfit.dana.models

data class ResponseLoanList(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val loans: List<Loan?>?
        ) {
            data class Loan(
                val ac_no: String?,
                val amount: String?,
                val approved: String?,
                val branch_id: String?,
                val comment: String?,
                val created_at: String?,
                val current: String?,
                val details: String?,
                val id: String?,
                val inr: String?,
                val method: String?,
                val org_distance: String?,
                val person_id: String?,
                val purpose: String?,
                val reference_id: String?,
                val status: String?,
                val tenor: String?
            )
        }
    }
}