package com.misfit.dana.models

data class ResponseLoanStatusChange(
    val response: Response?
) {
    data class Response(
        val `data`: String?,
        val error: Boolean?,
        val status: Int?
    )
}