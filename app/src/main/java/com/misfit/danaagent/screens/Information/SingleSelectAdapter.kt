package com.misfit.danaagent.screens.Information

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.misfit.danaagent.R
import kotlinx.android.synthetic.main.item_toggle_button.view.*

class SingleSelectAdapter(val context: Context, val onItemClick: (String) -> Unit) :
    RecyclerView.Adapter<SingleSelectAdapter.CustomView>() {

    val items = arrayListOf<String>()

    var selectedView: Button? = null

    fun resetItems(newItems: ArrayList<String>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        return CustomView(
            LayoutInflater.from(context).inflate(R.layout.item_toggle_button, null, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {

        holder.itemView.btn.text = items[position]

        holder.itemView.btn.setOnClickListener {

            onItemClick(items[position])

            if (selectedView == null) {
                holder.itemView.btn.setBackgroundResource(R.drawable.bg_button_selected)
                holder.itemView.btn.setTextColor(context.resources.getColor(android.R.color.white))

                selectedView = holder.itemView.btn
            } else {

//                if (selectedView == holder.itemView.btn) {
//
//                    holder.itemView.btn.setBackgroundResource(R.drawable.bg_button_unselected)
//                    holder.itemView.btn.setTextColor(context.resources.getColor(R.color.colorText))
//
//                    selectedView = null
//                } else {

                selectedView?.setBackgroundResource(R.drawable.bg_button_unselected)
                selectedView?.setTextColor(context.resources.getColor(R.color.colorText))

                holder.itemView.btn.setBackgroundResource(R.drawable.bg_button_selected)
                holder.itemView.btn.setTextColor(context.resources.getColor(android.R.color.white))

                selectedView = holder.itemView.btn
//                }


            }
        }


    }


    class CustomView(var view: View) : RecyclerView.ViewHolder(view)
}