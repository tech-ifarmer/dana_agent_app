package com.misfit.danaagent.screens.CameraNIDBack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.dana.data.RuntimeData
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.CameraNIDFront.NidFrontImageCheckActivity
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.activity_nid_back_camera.*

class NidBackCamera : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nid_back_camera)

        camera.setLifecycleOwner(this)

        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                val intent = Intent(this@NidBackCamera, NidBackImageCheckActivity::class.java )
                intent.putExtra("back", result.data)
                startActivity(intent)
            }
        })

        btn_tack_photo.setOnClickListener {
            camera.takePictureSnapshot()
        }

        btn_back.setOnClickListener { onBackPressed() }

    }
}