package com.parallax.showofff.api

import android.content.Intent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


class OkHttpClientSms {
    val okHttpClient: OkHttpClient
        get() {
            try {
                val builder = OkHttpClient.Builder()

                builder.connectTimeout(30, TimeUnit.SECONDS)
                builder.readTimeout(30, TimeUnit.SECONDS)
                builder.writeTimeout(30, TimeUnit.SECONDS)

                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.interceptors().add(interceptor)

                builder.addInterceptor { chain ->

                    val newRequest = chain.request().newBuilder()
//                        .addHeader("Content-Type", "text/plain")
//                        .addHeader("Accept", "application/json")
                        .build()

                    val response = chain.proceed(newRequest)

                    return@addInterceptor response
                }
                return builder.build()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
}