package com.misfit.danaagent.screens.CameraSelfie

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.misfit.dana.data.RuntimeData
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.CameraNIDFront.NidFrontImageCheckActivity
import com.misfit.danaagent.screens.HomeActivity
import com.misfit.danaagent.screens.Information.InformationActivity
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.activity_nid_front_camera.*
import kotlinx.android.synthetic.main.activity_nid_front_image_check.*
import kotlinx.android.synthetic.main.activity_nid_front_image_check.btn_back

class SelfieCamera : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selfie_camera)




        camera.setLifecycleOwner(this)

        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
            }
        })



        btn_next.setOnClickListener {
            startActivity(Intent(applicationContext, InformationActivity::class.java))
        }

        btn_again.setOnClickListener {
            onBackPressed()
        }

        btn_back.setOnClickListener { onBackPressed() }
    }
}