package com.misfit.danaagent.screens.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.misfit.dana.utils.AppUtils
import com.misfit.dana.utils.hide
import com.misfit.dana.utils.show
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.ApplicationListActivity
import com.misfit.danaagent.screens.DrwerMenu.MenuActivity
import com.misfit.danaagent.screens.Verification.PhoneNumberActivity
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {

    lateinit var v: View
    var borrowAmount = 500
    var duration = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home, container, false)

        v.tv_borrow_amount.text = AppUtils.getMoneyFormat(borrowAmount.toString())
        initListeners(v)

        initClick();

        return v
    }

    private fun initListeners(v: View) {


        v.seekBar.setOnSeekChangeListener(object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {
                borrowAmount = seekParams.progress
                v.tv_borrow_amount.text = AppUtils.getMoneyFormat(borrowAmount.toString())
                v.tv_loan.text = AppUtils.getMoneyFormat(borrowAmount.toString())
                v.tv_repayment.text = AppUtils.getMoneyFormat(borrowAmount.toString())
                v.tv_service_charge.text = "0.04"
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {
                if (duration != -1) setCharges()
            }
        })



        v.layout_7.setOnClickListener {
            duration = 7
            setCharges()
            v.layout_7.setBackgroundResource(R.drawable.bg_green_radius)
            v.layout_15.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_25.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_30.setBackgroundResource(R.drawable.bg_gray_filled_radius)
        }

        v.layout_15.setOnClickListener {
            duration = 15
            setCharges()
            v.layout_7.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_15.setBackgroundResource(R.drawable.bg_green_radius)
            v.layout_25.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_30.setBackgroundResource(R.drawable.bg_gray_filled_radius)
        }

        v.layout_25.setOnClickListener {
            duration = 25
            setCharges()
            v.layout_7.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_15.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_25.setBackgroundResource(R.drawable.bg_green_radius)
            v.layout_30.setBackgroundResource(R.drawable.bg_gray_filled_radius)
        }

        v.layout_30.setOnClickListener {
            duration = 30
            setCharges()
            v.layout_7.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_15.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_25.setBackgroundResource(R.drawable.bg_gray_filled_radius)
            v.layout_30.setBackgroundResource(R.drawable.bg_green_radius)
        }

        v.btn_apply.setOnClickListener {


        }


    }


    private fun setCharges() {
        v.layout_fees.show()
        v.tv_repayment_date.text = AppUtils.repayDate(duration)
        v.btn_apply.setBackgroundResource(R.drawable.bg_button_green)
        v.btn_apply.setTextColor(resources.getColor(android.R.color.white))

    }


    fun initClick() {

        val intent = Intent(activity, ApplicationListActivity::class.java )

        v.btn_menu.setOnClickListener {
            startActivity(Intent(activity, MenuActivity::class.java))
        }


        v.btn_pipeline.setOnClickListener {
            intent.putExtra("title", btn_pipeline.getText().toString())
            startActivity(intent)
        }

        v.btn_applied.setOnClickListener {
            intent.putExtra("title", btn_applied.getText().toString())
            startActivity(intent)
        }

        v.btn_approved.setOnClickListener {
            intent.putExtra("title", btn_approved.getText().toString())
            startActivity(intent)
        }

        v.btn_repayment.setOnClickListener {
            intent.putExtra("title", "REPAYMENT")
            startActivity(intent)
        }

        v.btn_disbursed.setOnClickListener {
            intent.putExtra("title", btn_disbursed.getText().toString())
            startActivity(intent)
        }

        v.btn_declined.setOnClickListener {
            intent.putExtra("title", btn_declined.getText().toString())
            startActivity(intent)
        }

        v.btn_new_application.setOnClickListener {
            v.borrow_layout.show()
            v.list_layout.hide()
            v.btn_new_application.hide()

        }

        v.btn_apply.setOnClickListener {
            startActivity(Intent(activity,PhoneNumberActivity::class.java))
        }



    }


}