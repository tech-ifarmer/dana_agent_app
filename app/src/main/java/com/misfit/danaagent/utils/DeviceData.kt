package com.misfit.dana.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.AsyncTask
import android.os.Build
import android.provider.CallLog
import android.provider.Telephony
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.misfit.dana.data.SPreferences
import com.misfit.dana.models.custom.SmsDataModel
import java.util.*


class DeviceData {

    fun readSms(context: Context, onDataCollected: (SmsDataModel) -> Unit) {
        AsyncTask.execute {

            val cal = Calendar.getInstance()
            cal.add(Calendar.MONTH, -1)
            val oneMonthAgo = cal.time.time

            val where =
                Telephony.Sms.DATE + " < " + Date().time + " AND " + Telephony.Sms.DATE + " > " + oneMonthAgo

            val cursor: Cursor =
                context.contentResolver.query(
                    Telephony.Sms.Inbox.CONTENT_URI,
                    null,
                    where,
                    null,
                    null
                )!!

            val smsDataModel = SmsDataModel()

            if (cursor.moveToFirst()) {

                val phone = SPreferences.getPhoneNo() ?: ""

                do {
                    smsDataModel.id.add(cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms._ID)))
                    smsDataModel.customer_id.add(phone)
                    smsDataModel.date.add(cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.DATE)))
                    smsDataModel.address.add(cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.ADDRESS)))
                    smsDataModel.body.add(cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.BODY)))

                } while (cursor.moveToNext())

                val a = Gson().toJson(smsDataModel)

                Log.d("DeviceData", a)

                onDataCollected(smsDataModel)

            } else {
                onDataCollected(smsDataModel)
            }
        }
    }

    fun countSms(context: Context, afterCount: (Int) -> Unit) {
        AsyncTask.execute {
            val cursor: Cursor =
                context.contentResolver.query(
                    Telephony.Sms.Inbox.CONTENT_URI,
                    null,
                    null,
                    null,
                    null
                )!!

            Log.d("DeviceData : inboxCount", cursor.count.toString())

            afterCount(cursor.count)
        }
    }


    fun countCall(context: Context, afterCount: (Int) -> Unit) {
        AsyncTask.execute {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_CALL_LOG
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val cursor: Cursor =
                    context.contentResolver.query(
                        CallLog.Calls.CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                    )!!

                Log.d("DeviceData : callCount", cursor.count.toString())

                afterCount(cursor.count)
            }


        }
    }

    fun getDeviceInfo(context: Context) {
        val brand = Build.MANUFACTURER
        Log.d("DeviceData", brand)

//        countSms(context) {}
//        countCall(context) {}
    }


}