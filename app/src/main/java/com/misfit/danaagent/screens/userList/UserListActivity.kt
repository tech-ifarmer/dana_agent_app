package com.misfit.danaagent.screens.userList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.misfit.danaagent.R
import com.misfit.danaagent.adapter.SampleAdapter
import kotlinx.android.synthetic.main.activity_application_list.*

class UserListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)


        val layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        rv.layoutManager = layoutManager
        val adapter = SampleAdapter(applicationContext, R.layout.item_user)
        rv.adapter = adapter


        btn_back.setOnClickListener {
            onBackPressed()
        }


    }


}