package com.misfit.dana.models

data class ResponseAccountList(
    val response: Response?
) {
    data class Response(
        val `data`: List<Data>,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val bank: String?,
            val branch: String?,
            val no: String?,
            val type: String?
        )
    }
}