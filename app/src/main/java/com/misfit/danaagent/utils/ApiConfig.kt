package com.parallax.showofff.utils

import com.misfit.dana.api.DeviceDataApi
import com.misfit.dana.api.ImageMatchingApi
import com.misfit.dana.api.LivenessApi
import com.misfit.dana.api.OcrApi
import com.parallax.showofff.api.DanaApiServices
import com.parallax.showofff.api.OkHttpClient
import com.parallax.showofff.api.OkHttpClientSms
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

var apiServices = ApiConfig().danaDanaApiServices
var imageMatchApiServices = ApiConfig().imageMatchApiServices
var livenessApiServices = ApiConfig().livenessApiServices
var ocrApiServices = ApiConfig().ocrApiServices
var deviceDataApiServices = ApiConfig().deviceDataApiServices

class ApiConfig {

   private var danaRetrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient().okHttpClient)
        .baseUrl(API_BASE_URL)
        .build()

    var danaDanaApiServices: DanaApiServices = danaRetrofit.create(DanaApiServices::class.java)

   private var imageMatchRetrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(okhttp3.OkHttpClient())
        .baseUrl("http://172.105.195.15:5001/")
        .build()

    var imageMatchApiServices: ImageMatchingApi = imageMatchRetrofit.create(ImageMatchingApi::class.java)


   private var livenessRetrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient().okHttpClient)
        .baseUrl("http://172.105.195.15:5252/api/")
        .build()

    var livenessApiServices: LivenessApi = livenessRetrofit.create(LivenessApi::class.java)

   private var ocrRetrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient().okHttpClient)
        .baseUrl("http://172.105.195.15:8080/")
        .build()

    var ocrApiServices: OcrApi = ocrRetrofit.create(OcrApi::class.java)

   private var deviceDataRetrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClientSms().okHttpClient)
        .baseUrl("http://100.26.209.153/core/")
        .build()

    var deviceDataApiServices: DeviceDataApi = deviceDataRetrofit.create(DeviceDataApi::class.java)
}


