package com.misfit.dana.api

import com.misfit.dana.models.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LivenessApi {
    @POST("inference")
    fun inference(@Body body: ArrayList<BodyLiveness.BodyLivenessItem>): Call<ResponseLiveness>
}