package com.misfit.danaagent.screens.psychomethic_question

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dana.money.screens.psychomethic_question.adapter.WithImageAdapter
import com.dana.money.screens.psychomethic_question.adapter.WithoutImageAdapter
import com.misfit.dana.data.RuntimeData
import com.misfit.dana.models.BodyLoanApply
import com.misfit.dana.models.BodySmsParsing
import com.misfit.dana.models.BodyUpdateDoc
import com.misfit.dana.utils.AppUtils
import com.misfit.dana.utils.DeviceData
import com.misfit.dana.utils.DialogUtil
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.LoanSubmitFinishActivity
import com.parallax.showofff.api.WebService
import com.parallax.showofff.utils.BASE64_PREFIX
import kotlinx.android.synthetic.main.activity_psychometric_question.*

class PsychometricQuestionActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_psychometric_question)

        btn_next.setOnClickListener {
            startActivity(
                Intent(
                    applicationContext,
                    LoanSubmitFinishActivity::class.java
                )
            )

        }
    }



}