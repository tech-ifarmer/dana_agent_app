package com.misfit.dana.models

data class ResponseImageMatching(
    val code: Int?,
    val distance: Double?,
    val message: String?,
    val result: Boolean?
)