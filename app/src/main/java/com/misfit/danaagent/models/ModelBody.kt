package com.misfit.dana.models

import com.misfit.dana.models.custom.SmsDataModel

class BodySendOtp {
    var phone: String? = null
}

class BodyOfferDetails {
    var id: Int? = null
}

class BodyClaimOffer {
    var id: Int? = null
}

class BodyLogin {
    var otp_id: String? = null
    var otp: String? = null
}

class BodyOcr {
    var b64NidFront: String? = null
    var b64NidBack: String? = null
}

class BodyRepayDocUpload {
    var loan_id: String? = null
    var ac_id: String? = null
    var ref: String? = null
    var doc: String? = null
}

class BodyLoanCharge {

//    {
//        "amount": 1000= null
//        "tenor": 7
//    }

    var amount: Int? = null
    var tenor: Int? = null
}


class BodyHkAddCustomer {
//    {
//        "name": "Abir Absar"= null
//        "phone": "01756264578"
//    }

    var name: String? = null
    var phone: String? = null
}

class BodyHkTransaction {
//    {
//        "customer_id": 1= null
//        "amount": 500= null
//        "date": "2020-05-20"= null
//        "type": "credit"
//    }

    var customer_id: Int? = null
    var amount: Int? = null
    var date: String? = null
    var type: String? = null
}

class BodyHkDetails {
//    {
//        "customer_id": 1
//    }

    var customer_id: Int? = null
}

class BodyLoanStatusChange {
//    {
//        "loan_id": "840",
//        "status": "1"
//    }

    var loan_id: String? = null
    var status: String? = null
}

class BodyLoanApply {
//    {
//        "tenor": "7",
//        "purpose": "Business",
//        "amount": "500",
//        "method" : "bkash",
//        "ac_no": "0181294872483",
//        "branch_id": "",
//        "ans": {
//        "0": "49",
//        "1": "39",
//        "2": "31",
//        "3": "5",
//        "4": "27",
//        "5": "82",
//        "6": "19",
//        "7": "35",
//        "8": "1",
//        "9": "7",
//        "10": "70",
//        "11": "9"
//    }
//    }

    var tenor: String? = null
    var purpose: String? = null
    var amount: String? = null
    var method: String? = null
    var ac_no: String? = null
    var branch_id: String? = null
    var lang: String? = null
    var ans: ArrayList<String>? = null
}

class BodyLoanAccountAdd {
//    {
//        "type": "bkash",
//        "branch_id": "",
//        "no": "017894665968"
//    }

    var type: String? = null
    var branch_id: String = ""
    var no: String? = null
    var loan_id : String? = null
}

class BodyUpdateProfession {


//    No Environment
//    BUILD
//    STD
//    "id_card":"", "faculty": "Science", "gpa": "1-2", "duration": "1-2", "school_type": "Public", "family_income": "10000", "father_edu": "P.S.C", "mother_edu": "P.S.C", "donor": "Family support"
//
//    Farmer
//    "farm_size" : "", "farm_type" : "", "exp" : "", "location" : ""
//
//    Business:
//    "type": "", "nature": "", "prev_business": "", "prev_duration": "", "cur_duration": "", "exp": "", "location": "", "no_emp": ""
//
//    Employee
//    {"emp_id": "","designation": "","company": "","prev_exp": "","cur_exp": "0-2","spouse_income": "0-5000","location": "Metropolitan","exp": "","emp_type": "Government","income_doc": "data:image/jpg;base64,/9j/4QAYRXhpZgAAS"}

    var prof_type: String? = ""

    var id_card: String? = ""
    var faculty: String? = ""
    var gpa: String? = ""
    var duration: String? = ""
    var school_type: String? = ""
    var family_income: String? = ""
    var father_edu: String? = ""
    var mother_edu: String? = ""
    var donor: String? = ""

    var farm_ownership: String? = ""
    var farm_size: String? = ""
    var farm_type: String? = ""

    var exp: String? = ""
    var location: String? = ""

    var type: String? = ""
    var nature: String? = ""
    var prev_business: String? = ""
    var prev_duration: String? = ""
    var cur_duration: String? = ""
    var no_emp: String? = ""

    var emp_id: String? = ""
    var designation: String? = ""
    var company: String? = ""
    var distributor: String? = ""
    var prev_exp: String? = ""
    var cur_exp: String? = ""
    var spouse_income: String? = ""
    var emp_type: String? = ""
    var income_doc: String? = ""
    var invoice: String? = ""

    var trade_lic: String? = ""
    var lang: String = "en"
}

class BodyLang {
    var lang: String = "en"
}

class BodySmsParsing {
    var data: SmsDataModel? = null
}

class BodyUpdateDoc {
//    {
//        "photo": ""
//        "nid_front": ""
//        "nid_back": ""
//        "nid_selfie": ""
//        "work_place": ""
//        "bank_statement": ""
//        "utility_bill": ""
//        "tax_doc": ""
//    }

    var photo = ""
    var nid_front = ""
    var nid_back = ""
    var nid_selfie = ""
    var work_place = ""
    var bank_statement = ""
    var utility_bill = ""
    var tax_doc = ""
}

class BodyUpdateBasic {
    var age: String? = ""
    var dob: String? = ""
    var earning_member: String? = ""
    var edu_level: String? = ""
    var email: String? = ""
    var father: String? = ""
    var mother: String? = ""
    var bg: String? = ""
    var extra_income: String? = ""
    var extra_income_type: String? = ""
    var fam_mem: String? = ""
    var gender: String? = ""
    var has_bank_ac: String? = ""
    var has_extra_income: String? = ""
    var has_membership: String? = ""
    var has_prev_loan: String? = ""
    var has_savings: String? = ""
    var home_ownership: String? = ""
    var home_type: String? = ""
    var income: String? = ""
    var married: String? = ""
    var membership: String? = ""
    var name: String? = ""
    var nid: String? = ""
    var no_loan: String? = ""
    var per_addr: String? = ""
    var pre_addr: String? = ""
    var prof_type: String? = ""
    var repayment: String? = ""
    var residence_location: String? = ""
    var savings: String? = ""
    var social_id: String? = ""
    var expense: String? = ""
    var years_residence: String? = ""
    var lang: String? = ""
}

data class BodyImageMatching(
    val gallery: Gallery?,
    val probe: Probe?
) {
    data class Gallery(
        val b64data: String?,
        val format: String?
    )

    data class Probe(
        val b64data: String?,
        val format: String?
    )
}

class BodyLiveness : ArrayList<BodyLiveness.BodyLivenessItem>() {
    data class BodyLivenessItem(
        val `data`: String?,
        val id: String?
    )
}