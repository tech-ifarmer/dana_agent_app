package com.misfit.danaagent.screens.Verification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.CameraSelfie.SelfieCamera
import com.misfit.danaagent.screens.CameraSelfie.SelfieInstructionActivity
import kotlinx.android.synthetic.main.activity_verify_info.*

class VerifyInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_info)

        btn_next.setOnClickListener {
            startActivity(Intent(applicationContext, SelfieInstructionActivity::class.java))
        }
    }
}