package com.misfit.danaagent.screens.PermissionsActivity

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.AgentLogin.LoginActivity
import kotlinx.android.synthetic.main.activity_permissions.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

class PermissionsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permissions)


        btn_agree.setOnClickListener {
            askPermissions()
        }


    }

    @AfterPermissionGranted(999)
    private fun askPermissions() {
        val perms = arrayOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_SMS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (EasyPermissions.hasPermissions(this, *perms)) {
            Intent(applicationContext, LoginActivity::class.java).let {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        } else {
            EasyPermissions.requestPermissions(
                this, "Please give all permissions",
                999, *perms
            )
        }
    }

}