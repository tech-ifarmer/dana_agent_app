package com.parallax.showofff.api

import com.misfit.dana.models.*
import retrofit2.Call
import retrofit2.http.*

interface DanaApiServices {

    @POST("user/bypass")
    fun userBypass(@Body body: BodySendOtp): Call<ResponseLogin>

    @POST("user/sendotp")
    fun sendOtp(@Body body: BodySendOtp): Call<ResponseSendOtp>

    @POST("user/login")
    fun login(@Body body: BodyLogin): Call<ResponseLogin>

    @POST("loan/charge")
    fun loanCharge(@Body body: BodyLoanCharge): Call<ResponseLoanCharge>

    @POST("loan/apply")
    fun loanApply(@Body body: BodyLoanApply): Call<ResponseLoanApply>

    @POST("user/update_basic")
    fun updateBasic(@Body body: BodyUpdateBasic): Call<ResponseUpdateBasic>

    @POST("user/update_profession")
    fun updateProfession(@Body body: BodyUpdateProfession): Call<ResponseUpdateProfession>

    @POST("loan/repay")
    fun repayUploadDoc(@Body body: BodyRepayDocUpload): Call<ResponseUpdateProfession>

    @POST("user/update_doc")
    fun updateDoc(@Body body: BodyUpdateDoc): Call<ResponseUpdateDoc>

    @POST("user/status")
    fun userStatus(): Call<ResponseUserStatus>

    @POST("settings/refer_msg")
    fun referMessage(): Call<ResponseReferMessage>

    @POST("user")
    fun userProfile(): Call<ResponseUserProfile>

    @POST("variables")
    fun variables(@Body body: BodyLang): Call<ResponseVariable>

    @POST("loan/getques")
    fun getQuestions(@Body body: BodyLang): Call<ResponseQuestions>

    @POST("offer")
    fun getOffer(): Call<ResponseOffer>

    @POST("loan_ac")
    fun getLoanAccountList(): Call<ResponseAccountList>

    @POST("offer/details")
    fun getOfferDetails(@Body body: BodyOfferDetails): Call<ResponseOfferDetails>

    @POST("offer/claim")
    fun offerClaim(@Body body: BodyClaimOffer): Call<ResponseClaimOffer>

    @POST("repay_ac")
    fun getRepayAc(): Call<ResponseRepayAc>

    @POST("hishab")
    fun getHishabSummery(): Call<ResponseHkSummery>

    @POST("hishab/add")
    fun addCustomer(@Body body: BodyHkAddCustomer): Call<ResponseHkAddCustomer>

    @POST("hishab/trxn")
    fun hishabTransaction(@Body body: BodyHkTransaction): Call<ResponseHkTransaction>

    @POST("hishab/details")
    fun hishabDetails(@Body body: BodyHkDetails): Call<ResponseHkDetails>

    @POST("loan/changestatus")
    fun loanStatusChange(@Body body: BodyLoanStatusChange): Call<ResponseLoanStatusChange>

    @POST("loan/status")
    fun loanStatus(): Call<ResponseLoanStatus>

    @POST("loan")
    fun loanList(): Call<ResponseLoanList>

    @POST("banks")
    fun bankList(): Call<ResponseBankList>

    @POST("log/messages")
    fun logMessage(): Call<ResponseLogMessage>

    @POST("log/statements")
    fun logStatements(): Call<ResponseLogStatement>

    @POST("banks/{id}")
    fun getBranch(@Path("id") id: String): Call<ResponseBranch>

    @POST("loan_ac/add")
    fun addLoanAccount(@Body body: BodyLoanAccountAdd): Call<ResponseAccountAdd>

}