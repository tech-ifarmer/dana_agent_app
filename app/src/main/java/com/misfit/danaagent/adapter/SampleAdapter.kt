package com.misfit.danaagent.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class SampleAdapter(var context : Context, var resId : Int, var onClickItem : (()->Unit)? = null) : RecyclerView.Adapter<SampleAdapter.CustomView>() {

    var items = arrayListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        return CustomView(LayoutInflater.from(context).inflate(resId, null, false))
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {
        holder.itemView.setOnClickListener { onClickItem?.let { it() } }

    }


    class CustomView(var view : View) : RecyclerView.ViewHolder(view){

    }
}