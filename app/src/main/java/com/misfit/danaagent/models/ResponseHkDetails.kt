package com.misfit.dana.models

data class ResponseHkDetails(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val balance: String?,
            val msg: String?,
            val name: String?,
            val trxns: ArrayList<Trxn>
        ) {
            data class Trxn(
                val amount: String?,
                val date: String?,
                val type: String?
            )
        }
    }
}