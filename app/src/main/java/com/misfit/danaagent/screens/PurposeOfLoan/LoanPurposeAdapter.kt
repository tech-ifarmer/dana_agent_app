package com.misfit.danaagent.screens.PurposeOfLoan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.misfit.danaagent.R
import kotlinx.android.synthetic.main.item_radio_button.view.*

class LoanPurposeAdapter(var context: Context, var onClickItem: ((String) -> Unit)? = null) :
    RecyclerView.Adapter<LoanPurposeAdapter.CustomView>() {

    var items = arrayListOf<String>()

    var selectedRadio: RadioButton? = null

    fun resetItems(newItems: ArrayList<String>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        return CustomView(
            LayoutInflater.from(context).inflate(R.layout.item_radio_button, null, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {

        holder.itemView.rb.text = items[position]

        holder.itemView.rb.setOnClickListener {

            if (selectedRadio == null) {
                selectedRadio = holder.itemView.rb

//                holder.itemView.rb.isChecked = true
            } else {
                if (selectedRadio != holder.itemView.rb) {
                    selectedRadio?.isChecked = false
//                    holder.itemView.rb.isChecked = true

                    selectedRadio = holder.itemView.rb
                }
            }

            onClickItem?.let { it(items[position]) }
        }

    }


    class CustomView(var view: View) : RecyclerView.ViewHolder(view)
}