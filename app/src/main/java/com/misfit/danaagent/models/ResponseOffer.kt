package com.misfit.dana.models

data class ResponseOffer(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val offers: ArrayList<Offer>
        ) {
            data class Offer(
                val company: String?,
                val created_at: String?,
                val details: String?,
                val end: String?,
                val id: String,
                val photo: String?,
                val start: String?,
                val status: String?,
                val title: String?,
                val type: String?
            )
        }
    }
}