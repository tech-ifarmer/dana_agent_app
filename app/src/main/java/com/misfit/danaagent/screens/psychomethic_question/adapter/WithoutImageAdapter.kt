package com.dana.money.screens.psychomethic_question.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.misfit.dana.models.ResponseQuestions
import com.misfit.danaagent.R
import kotlinx.android.synthetic.main.item_psychometric_ques_without_image.view.*

class WithoutImageAdapter(
    val context: Context,
    val onItemClick: (ResponseQuestions.Response.Data.Question.An) -> Unit
) :
    RecyclerView.Adapter<WithoutImageAdapter.CustomView>() {

    val items = arrayListOf<ResponseQuestions.Response.Data.Question.An>()

    var selectedView: RadioButton? = null

    fun resetItems(newItems: ArrayList<ResponseQuestions.Response.Data.Question.An>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        return CustomView(
            LayoutInflater.from(context)
                .inflate(R.layout.item_psychometric_ques_without_image, null, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {

        holder.itemView.rb_ans.text = items[position].ans

        holder.itemView.rb_ans.setOnClickListener {

            onItemClick(items[position])

            if (selectedView == null) {
                holder.itemView.rb_ans.isChecked = true

                selectedView = holder.itemView.rb_ans
            } else {

                selectedView?.isChecked = false
                holder.itemView.rb_ans.isChecked = true


                selectedView = holder.itemView.rb_ans
            }
        }


    }


    class CustomView(var view: View) : RecyclerView.ViewHolder(view)
}