package com.misfit.danaagent.screens.CameraNIDFront

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.misfit.dana.data.RuntimeData
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.ApplicationListActivity
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.activity_nid_front_camera.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class NidFrontCamera : LocalizationActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nid_front_camera)
        camera.setLifecycleOwner(this)

        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {

                val intent = Intent(this@NidFrontCamera, NidFrontImageCheckActivity::class.java )
                    intent.putExtra("front", result.data)
                    startActivity(intent)
            }
        })

        btn_tack_photo.setOnClickListener {
            camera.takePictureSnapshot()
        }

        btn_back.setOnClickListener { onBackPressed() }

    }
}