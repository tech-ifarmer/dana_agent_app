package com.misfit.dana.models

data class ResponseClaimOffer(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val msg: String?
        )
    }
}