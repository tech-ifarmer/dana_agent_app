package com.misfit.dana.models

data class ResponseUserStatus(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val balance: String?,
            val num_paid: Double?,
            val limit: Double?,
            val person: Person?,
            val registered: String?,
            val repayment: String?,
            val score: Double?
        ) {
            data class Person(
                val name: String?,
                val phone: String?
            )
        }
    }
}