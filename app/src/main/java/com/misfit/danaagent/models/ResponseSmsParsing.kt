package com.misfit.dana.models

data class ResponseSmsParsing(
    val `data`: List<Data?>?,
    val message: String?,
    val success: Boolean?
) {
    data class Data(
        val address: String?,
        val balance: Double?,
        val bank_to_mobile_recharge: Double?,
        val card_payment: Double?,
        val card_purchase: Double?,
        val credit: Double?,
        val date: Long?,
        val debit: Double?,
        val deposit: Double?,
        val fund_transfer_in: Double?,
        val fund_transfer_out: Double?,
        val id: String?,
        val interest_credit: Double?,
        val withdrawal: Double?
    )
}