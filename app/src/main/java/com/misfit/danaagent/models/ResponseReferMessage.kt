package com.misfit.dana.models

data class ResponseReferMessage(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val name: String?,
            val value: String?
        )
    }
}