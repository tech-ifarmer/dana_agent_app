package com.parallax.showofff.utils

const val API_BASE_URL = "http://softcell.yearstech.com/demo/dana/api/"
const val IMAGE_BASE_URL = "http://softcell.yearstech.com/demo/dana/contents/"

const val TAKA_SYMBLE = "৳"

const val BASE64_PREFIX = "data:image/jpg;base64,"

enum class ProfessionType {
    Student,
    Farmer,
    Business,
    Employee
}

enum class LoanStatus(val value: String) {
    Pending("0"),
    Accepted("1"),
    Approved("2"),
    Declined("3"),
    Disbursed("4"),
    Paid("5"),
}


