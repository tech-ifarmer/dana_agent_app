package com.misfit.danaagent.screens

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.misfit.danaagent.adapter.SampleAdapter
import com.misfit.danaagent.R
import com.misfit.danaagent.adapter.DeclinedListAdapter
import kotlinx.android.synthetic.main.activity_application_list.*

class ApplicationListActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_application_list)


        val layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        rv.layoutManager = layoutManager


        val textView: TextView = findViewById(R.id.title) as TextView

        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            val message = bundle.getString("title") // 1
            textView.text = message

            if (message == "PIPELINE") {
                val adapter = SampleAdapter(applicationContext, R.layout.item_pipeline)
                rv.adapter = adapter
            } else if (message == "APPLIED") {
                val adapter = SampleAdapter(applicationContext, R.layout.item_applied)
                rv.adapter = adapter
            } else if (message == "APPROVED") {
                val adapter = SampleAdapter(applicationContext, R.layout.item_approved)
                rv.adapter = adapter
            } else if (message == "REPAYMENT") {
                val adapter = SampleAdapter(applicationContext, R.layout.item_repayment)
                rv.adapter = adapter
            } else if (message == "DISBURSED") {
                val adapter = SampleAdapter(applicationContext, R.layout.item_disbursed)
                rv.adapter = adapter
            } else {
                val adapter = DeclinedListAdapter(list())
                rv.adapter = adapter
            }


        }



        btn_back.setOnClickListener {
            onBackPressed()
        }

    }

    fun list(): ArrayList<String> {
        var mList = ArrayList<String>()

        mList.add("test")
        mList.add("test")
        mList.add("test")
        mList.add("test")
        return mList

    }


}