package com.misfit.dana.models.custom

class LoanApplyModel {

    var loanAmount: Double? = null
    var loanDurationInDay: Int? = null

    var purpose: String? = null

    var dataNidFront: ByteArray? = null
    var dataNidBack: ByteArray? = null
    var dataPhotoWithNid: ByteArray? = null

    var psychometricAns: ArrayList<String>? = null


}