package com.misfit.danaagent.screens.AgentLogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.DanaLoanSteps.DanaStepsActivity
import com.misfit.danaagent.screens.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class AgentOTPActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agent_o_t_p)

        btn_proceed.setOnClickListener {

            val i = Intent(this, HomeActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)



        }


    }
}