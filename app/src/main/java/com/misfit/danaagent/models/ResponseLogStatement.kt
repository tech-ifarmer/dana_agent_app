package com.misfit.dana.models

data class ResponseLogStatement(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val statements: ArrayList<Statement>
        ) {
            data class Statement(
                val ac_no: String?,
                val amount: String?,
                val comment: String?,
                val date: String?,
                val title: String?,
                val type: String?
            )
        }
    }
}