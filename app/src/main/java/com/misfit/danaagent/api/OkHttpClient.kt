package com.parallax.showofff.api

import android.content.Intent
import android.util.Log
import com.misfit.dana.Dana
import com.misfit.dana.data.SPreferences
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


class OkHttpClient {
    val okHttpClient: OkHttpClient
        get() {
            try {
                val builder = OkHttpClient.Builder()

                builder.connectTimeout(30, TimeUnit.SECONDS)
                builder.readTimeout(30, TimeUnit.SECONDS)
                builder.writeTimeout(30, TimeUnit.SECONDS)

                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.interceptors().add(interceptor)

                builder.addInterceptor { chain ->

                    val authorization = SPreferences.getUserToken()
                    Log.d("UserAUTH", "" + authorization)

                    val newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer $authorization" ?: "")
//                        .addHeader("Accept", "application/json")
                        .build()

                    val response = chain.proceed(newRequest)

                    if (response.code() == 401) {
                        //Signout user
//                        SPreferences.setUserToken(null)
//
//                        val intent = Intent(
//                            Dana.instance.applicationContext,
//                            WelcomeActivity::class.java
//                        )
//                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
//                        Dana.instance.applicationContext.startActivity(intent)

                        return@addInterceptor response
                    }

                    return@addInterceptor response
                }
                return builder.build()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
}