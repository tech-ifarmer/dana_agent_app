package com.misfit.dana.models.custom

 class SmsDataModel {
     var id: ArrayList<String> = arrayListOf()
     var date: ArrayList<String> = arrayListOf()
     var customer_id: ArrayList<String> = arrayListOf()
     var address: ArrayList<String> = arrayListOf()
     var body: ArrayList<String> = arrayListOf()
 }