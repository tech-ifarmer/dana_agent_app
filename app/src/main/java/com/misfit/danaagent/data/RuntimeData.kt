package com.misfit.dana.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.misfit.dana.api.OcrApi
import com.misfit.dana.models.*
import com.misfit.dana.models.custom.LoanApplyModel

object RuntimeData {

    var hasCheckLoanStatus = false

    var loanApplyModel: LoanApplyModel? = null
    var bodyUpdateBasic: BodyUpdateBasic? = null
    var bodyUpdateProfession: BodyUpdateProfession? = null
    var nidOcr : ResponseOcr? = null

    var responseVariable: ResponseVariable? = null
    var responseQuestions: ResponseQuestions? = null
    var objUserProfile = MutableLiveData<ResponseUserProfile>()


}