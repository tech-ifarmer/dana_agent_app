package com.misfit.danaagent.screens.Verification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.DanaLoanSteps.DanaStepsActivity
import kotlinx.android.synthetic.main.activity_login.*

class OTPActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_o_t_p)



        btn_proceed.setOnClickListener {
            startActivity(
                Intent(applicationContext, DanaStepsActivity::class.java)
            )
        }
    }
}