package com.misfit.danaagent.screens.CameraSelfie

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.misfit.danaagent.R
import kotlinx.android.synthetic.main.activity_selfie_instruction.*

class SelfieInstructionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selfie_instruction)

        btn_take_selfie.setOnClickListener {
            startActivity(Intent(applicationContext, SelfieCamera::class.java))
        }

        btn_back.setOnClickListener { onBackPressed() }

    }
}