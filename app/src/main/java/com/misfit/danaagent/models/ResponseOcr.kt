package com.misfit.dana.models

data class ResponseOcr(
    val errorCode: Any?,
    val passKyc: String?,
    val voter: Voter?
) {
    data class Voter(
        val dob: String?,
        val father: String?,
        val fatherEn: String?,
        val gender: String?,
        val mother: String?,
        val motherEn: String?,
        val name: String?,
        val nameEn: String?,
        val permanentAddress: String?,
        val permanentAddressEn: String?,
        val photo: String?,
        val presentAddress: String?,
        val presentAddressEn: String?,
        val spouse: String?,
        val spouseEn: String?
    )
}