package com.misfit.dana.models

data class ResponseBranch(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val bank: Bank?
        ) {
            data class Bank(
                val branches: List<Branche>,
                val code: String?,
                val id: String?,
                val name: String?
            ) {
                data class Branche(
                    val district: String?,
                    val id: String?,
                    val name: String?
                )
            }
        }
    }
}