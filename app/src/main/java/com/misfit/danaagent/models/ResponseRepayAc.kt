package com.misfit.dana.models

data class ResponseRepayAc(
    val response: Response?
) {
    data class Response(
        val `data`: ArrayList<Data>,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val ac_no: String?,
            val bank: String?,
            val branch: String?,
            val phone: String?,
            val type: String?
        )
    }
}