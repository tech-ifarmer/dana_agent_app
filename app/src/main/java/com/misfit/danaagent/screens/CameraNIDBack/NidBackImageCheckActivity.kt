package com.misfit.danaagent.screens.CameraNIDBack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import com.bumptech.glide.Glide
import com.misfit.dana.data.RuntimeData
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.Verification.VerifyInfoActivity
import com.parallax.showofff.api.WebService
import kotlinx.android.synthetic.main.activity_nid_front_image_check.*

class NidBackImageCheckActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nid_back_image_chcek)


        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            val message = bundle.getByteArray("back")
            Glide.with(this).load(message).into(iv)
        }


        btn_next.setOnClickListener {
            startActivity(Intent(applicationContext, VerifyInfoActivity::class.java))
        }


        btn_again.setOnClickListener {
            onBackPressed()
        }

        btn_back.setOnClickListener { onBackPressed() }

    }
}