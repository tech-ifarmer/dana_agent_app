package com.misfit.dana.models

data class ResponseQuestions(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val questions: ArrayList<Question>
        ) {
            data class Question(
                val ans: ArrayList<An>,
                val has_img: String?,
                val id: String?,
                val num_ans: String?,
                val title: String?
            ) {
                data class An(
                    val ans: String?,
                    val comment: String?,
                    val id: String
                )
            }
        }
    }
}