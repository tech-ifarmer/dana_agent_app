package com.misfit.danaagent.screens.IncomeExpanse

import android.content.Intent
import android.os.Bundle
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.misfit.dana.data.RuntimeData
import com.misfit.dana.utils.hide
import com.misfit.dana.utils.show
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.Information.SingleSelectAdapter
import com.misfit.danaagent.screens.PurposeOfLoan.PurposeOfLoanActivity
import kotlinx.android.synthetic.main.activity_income_expence_info.*

class IncomeExpenceInfoActivity : AppCompatActivity() {
    val totalExpense = RuntimeData.responseVariable?.response?.data?.data?.expense
    val totalIncome = RuntimeData.responseVariable?.response?.data?.data?.income


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_income_expence_info)


        val popupIncome = PopupMenu(this, layout_monthly_income)
        totalIncome?.forEach {
            popupIncome.menu.add(it)
        }

        popupIncome.setOnMenuItemClickListener {
            RuntimeData.bodyUpdateBasic?.income = it.title.toString()
            tv_monthly_income.text = it.title
            true
        }

        val popupExpense = PopupMenu(this, layout_monthly_expense)
        totalExpense?.forEach {
            popupExpense.menu.add(it)
        }

        popupExpense.setOnMenuItemClickListener {
            RuntimeData.bodyUpdateBasic?.expense = it.title.toString()
            tv_monthly_expense.text = it.title
            true
        }


        rv_prev_loan.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_prev_loan.adapter = SingleSelectAdapter(applicationContext) {
            when (it) {
                "Yes" -> {
                    layout_previous_loan.show()
                }
                "No" -> {
                    layout_previous_loan.hide()
                }
            }
            RuntimeData.bodyUpdateBasic?.has_prev_loan = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.has_prev_loan?.let { list ->
                resetItems(list)
            }
        }

        rv_home_ownership.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_home_ownership.adapter = SingleSelectAdapter(applicationContext) {
            RuntimeData.bodyUpdateBasic?.home_ownership = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.home_ownership?.let { list ->
                resetItems(list)
            }
        }

        rv_family_members.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_family_members.adapter = SingleSelectAdapter(applicationContext) {
            RuntimeData.bodyUpdateBasic?.fam_mem = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.fam_mem?.let { list ->
                resetItems(list)
            }
        }

        rv_residence_location.layoutManager = GridLayoutManager(applicationContext, 2)
        rv_residence_location.adapter = SingleSelectAdapter(applicationContext) {
            RuntimeData.bodyUpdateBasic?.residence_location = it
        }.apply {
            RuntimeData.responseVariable?.response?.data?.data?.residence_location?.let { list ->
                resetItems(list)
            }
        }


        layout_monthly_income.setOnClickListener {
            popupIncome.show()
        }

        layout_monthly_expense.setOnClickListener {
            popupExpense.show()
        }



        btn_next.setOnClickListener {
            startActivity(Intent(applicationContext, PurposeOfLoanActivity::class.java))

        }

    }

}