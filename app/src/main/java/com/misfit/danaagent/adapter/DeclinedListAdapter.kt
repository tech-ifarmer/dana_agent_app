package com.misfit.danaagent.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity

import androidx.recyclerview.widget.RecyclerView
import com.misfit.danaagent.R
import com.misfit.danaagent.screens.HomeActivity
import com.misfit.danaagent.screens.psychomethic_question.PsychometricQuestionActivity


class DeclinedListAdapter(val userList: ArrayList<String>) : RecyclerView.Adapter<DeclinedListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeclinedListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_declined, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: DeclinedListAdapter.ViewHolder, position: Int) {

        holder.reapply.setOnClickListener {

            val context=holder.reapply.context
            val intent = Intent( context, PsychometricQuestionActivity::class.java)
            context.startActivity(intent)
            
        }




    }



    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val reapply : Button = itemView.findViewById(R.id.re_apply)
    }
}

