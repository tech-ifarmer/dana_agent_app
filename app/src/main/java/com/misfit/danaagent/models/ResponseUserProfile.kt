package com.misfit.dana.models

data class ResponseUserProfile(
    val response: Response?
) {
    data class Response(
        val `data`: Data?,
        val error: Boolean?,
        val status: Int?
    ) {
        data class Data(
            val docs: Docs?,
            val info: Info?
//            val job: Job?
        ) {
            data class Docs(
                val bank_statement: String?,
                val comment: String?,
                val created_at: String?,
                val has_bank_statement: String?,
                val has_nid: String?,
                val has_tax_doc: String?,
                val has_utility_bill: String?,
                val has_work_place: String?,
                val id: String?,
                val nid_back: String?,
                val nid_front: String?,
                val nid_selfie: String?,
                val person_id: String?,
                val photo: String?,
                val status: String?,
                val tax_doc: String?,
                val utility_bill: String?,
                val work_place: String?
            )

            data class Info(
                val age: Any?,
                val comment: Any?,
                val created_at: String?,
                val dob: String?,
                val earning_member: Any?,
                val edu_level: Any?,
                val email: String?,
                val expense: String?,
                val extra_income: Any?,
                val extra_income_type: Any?,
                val fam_mem: Any?,
                val gender: String?,
                val has_bank_ac: String?,
                val has_extra_income: String?,
                val has_membership: String?,
                val has_prev_loan: String?,
                val has_savings: String?,
                val home_ownership: Any?,
                val home_type: Any?,
                val id: String?,
                val income: Any?,
                val job: String?,
                val married: Any?,
                val membership: Any?,
                val name: String?,
                val nid: Any?,
                val no_loan: String?,
                val per_addr: Any?,
                val phone: String?,
                val photo: String?,
                val pre_addr: String?,
                val prof_type: String?,
                val registered: String?,
                val repayment: String?,
                val residence_location: String?,
                val savings: Any?,
                val social_id: Any?,
                val status: Any?,
                val years_residence: String?
            )

            data class Job(
                val comment: String?,
                val created_at: String?,
                val donor: String?,
                val duration: String?,
                val faculty: String?,
                val family_income: String?,
                val father_edu: String?,
                val gpa: String?,
                val has_id_card: String?,
                val id: String?,
                val id_card: String?,
                val job: String?,
                val mother_edu: String?,
                val person_id: String?,
                val school_type: String?,
                val status: String?
            )

        }
    }
}
